import React from "react";
import ReactDOM from "react-dom";
import {BrowserRouter as RouterProvider} from "react-router-dom";
import {ApolloClient, InMemoryCache, ApolloProvider} from "@apollo/client";

import App from "./App";

const apolloClient = new ApolloClient({
  uri: "/graphql",
  credentials: "include",
  cache: new InMemoryCache()
});

ReactDOM.render(
  <React.StrictMode>
    <ApolloProvider client={apolloClient}>
      <RouterProvider>
        <App/>
      </RouterProvider>
    </ApolloProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
