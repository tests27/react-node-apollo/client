import React from "react";
import { Route } from "react-router-dom";

import Home from "./pages/Home";
import Login from "./pages/Login";
import Registration from "./pages/Registration";

import "./assets/styles/main.scss";
import PrivateRoute from "./components/PrivateRoute";

function App() {
  return (
    <div className="App">
      <main className="page">
        <Route path="/login" component={Login} />
        <Route path="/registration" component={Registration} />
        <PrivateRoute path="/" exact component={Home} />
      </main>
    </div>
  );
}

export default App;
