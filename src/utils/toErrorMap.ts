import { FieldError } from "../generated/graphql";

export default function toErrorMap(errors: FieldError[]) {
  return errors.map((err) => err.message);
}
