import React, { FormEvent, useState } from "react";
import LogoImg from "../assets/img/logo.svg";
import UserIcon from "../assets/img/user.svg";
import KeyIcon from "../assets/img/key.svg";
import { Link, useHistory } from "react-router-dom";
import { useRegisterMutation } from "../generated/graphql";
import toErrorMap from "../utils/toErrorMap";

const validateRegisterForm = (
  username: string,
  passowrd: string,
  passowrd2: string
) => {
  let isValid = true;
  const errors = [];

  if (!username.length || !passowrd.length || !passowrd2.length) {
    isValid = false;
    errors.push("Все поля обязательны");
  }

  if (passowrd.length < 6) {
    isValid = false;
    errors.push("Минимальная длина пароля - 6 символов");
  }

  if (passowrd !== passowrd2) {
    isValid = false;
    errors.push("Пароли не совпадают");
  }

  return { isValid, errors };
};

function Registration() {
  const history = useHistory();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [errors, setErrors] = useState<string[]>([]);

  const [register] = useRegisterMutation({
    variables: {
      username,
      password
    }
  });

  const submitHandler = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const { isValid, errors } = validateRegisterForm(
      username,
      password,
      password2
    );
    if (!isValid) {
      return setErrors(errors);
    }

    const response = await register();
    if (!!response?.data?.register.errors) {
      setErrors(toErrorMap(response?.data?.register.errors));
    }

    history.push("/login");
  };

  return (
    <section className="form-page">
      <div className="form-page__box">
        <img src={LogoImg} alt="logo" className="form-page__logo" />
        <form
          className="form-page__form form"
          autoComplete="off"
          noValidate
          onSubmit={submitHandler}
        >
          {errors.map((err, idx) => (
            <p key={`${err}_${idx}`} className="form__error">
              {err}
            </p>
          ))}
          <label className="form__control input">
            <input
              type="text"
              className="input__field"
              placeholder="Логин"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
            <div className="input__icon-wrap">
              <img src={UserIcon} alt="login" className="input__icon" />
            </div>
          </label>
          <label className="form__control input">
            <input
              type="password"
              className="input__field"
              placeholder="Пароль"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <div className="input__icon-wrap">
              <img src={KeyIcon} alt="password" className="input__icon" />
            </div>
          </label>
          <label className="form__control input">
            <input
              type="password"
              className="input__field"
              placeholder="Повторите пароль"
              value={password2}
              onChange={(e) => setPassword2(e.target.value)}
            />
            <div className="input__icon-wrap">
              <img src={KeyIcon} alt="password" className="input__icon" />
            </div>
          </label>
          <button className="form__btn btn">Регистрация</button>
        </form>
        <Link to="/login" className="form-page__hint link">
          Уже есть аккаунт? Войти
        </Link>
      </div>
    </section>
  );
}

export default Registration;
