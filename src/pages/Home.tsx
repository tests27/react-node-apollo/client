import React, { FormEvent, useEffect, useState } from "react";
import LogoImg from "../assets/img/logo.svg";
import ArrayIcon from "../assets/img/array.svg";
import NumberIcon from "../assets/img/numpad.svg";
import { useSolveLazyQuery } from "../generated/graphql";

function Home() {
  const [numbers, setNumbers] = useState("");
  const [arr, setArr] = useState<number[]>([]);
  const [n, setN] = useState("");

  useEffect(() => {
    setArr(
      numbers
        .replace(/ /g, "")
        .replace(/,$/g, "")
        .split(",")
        .filter((el) => el.length)
        .map((el) => +el)
    );
  }, [numbers]);

  const [solve, { data }] = useSolveLazyQuery({
    variables: {
      arr: arr,
      n: +n
    }
  });

  const submitHandler = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    solve();
  };

  return (
    <section className="form-page">
      <div className="form-page__box">
        <img src={LogoImg} alt="logo" className="form-page__logo" />
        <form
          className="form-page__form form"
          autoComplete="off"
          noValidate
          onSubmit={submitHandler}
        >
          <label className="form__control input">
            <input
              type="text"
              className="input__field"
              placeholder="Числа массива (через запятую)"
              value={numbers}
              onChange={(e) => setNumbers(e.target.value)}
            />
            <div className="input__icon-wrap">
              <img src={ArrayIcon} alt="login" className="input__icon" />
            </div>
          </label>
          <label className="form__control input">
            <input
              type="number"
              className="input__field"
              placeholder="Число N"
              value={n}
              onChange={(e) => setN(e.target.value)}
            />
            <div className="input__icon-wrap">
              <img src={NumberIcon} alt="password" className="input__icon" />
            </div>
          </label>
          <button className="form__btn btn">Расчитать</button>
          {!!n && <p className="form__result">{JSON.stringify(data?.solve)}</p>}
        </form>
      </div>
    </section>
  );
}

export default Home;
