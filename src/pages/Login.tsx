import React, { FormEvent, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import LogoImg from "../assets/img/logo.svg";
import UserIcon from "../assets/img/user.svg";
import KeyIcon from "../assets/img/key.svg";
import { useLoginMutation } from "../generated/graphql";
import toErrorMap from "../utils/toErrorMap";

const validateLoginForm = (username: string, passowrd: string) => {
  let isValid = true;
  const errors = [];

  if (!username.length || !passowrd.length) {
    isValid = false;
    errors.push("Все поля обязательны");
  }

  return { isValid, errors };
};

function Login() {
  const history = useHistory();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [errors, setErrors] = useState<string[]>([]);

  const [login, { data }] = useLoginMutation({
    variables: {
      username,
      password
    }
  });

  const submitHandler = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const { isValid, errors } = validateLoginForm(username, password);
    if (!isValid) {
      return setErrors(errors);
    }

    const response = await login();
    if (!!response?.data?.login.errors) {
      return setErrors(toErrorMap(response?.data?.login.errors));
    }
    localStorage.setItem("isAuth", "true");
    // history.push("/");
    setTimeout(() => history.push("/"), 0);
  };

  return (
    <section className="form-page">
      <div className="form-page__box">
        <img src={LogoImg} alt="logo" className="form-page__logo" />
        <form
          className="form-page__form form"
          autoComplete="off"
          noValidate
          onSubmit={submitHandler}
        >
          {errors.map((err, idx) => (
            <p key={`${err}_${idx}`} className="form__error">
              {err}
            </p>
          ))}
          <label className="form__control input">
            <input
              type="text"
              className="input__field"
              placeholder="Логин"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
            <div className="input__icon-wrap">
              <img src={UserIcon} alt="login" className="input__icon" />
            </div>
          </label>
          <label className="form__control input">
            <input
              type="password"
              className="input__field"
              placeholder="Пароль"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <div className="input__icon-wrap">
              <img src={KeyIcon} alt="password" className="input__icon" />
            </div>
          </label>
          <button className="form__btn btn">Войти</button>
        </form>
        <Link to="/registration" className="form-page__hint link">
          Нет аккаунта? Зарегистрироваться
        </Link>
      </div>
    </section>
  );
}

export default Login;
