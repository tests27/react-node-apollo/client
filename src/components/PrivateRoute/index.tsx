import React from "react";
import { Redirect, Route } from "react-router-dom";

interface PrivateRouteProps {
  exact?: boolean;
  path: string;
  component: React.ComponentType<any>;
}

function PrivateRoute({ component: MyComponent, ...rest }: PrivateRouteProps) {
  return (
    <Route
      {...rest}
      render={(props) =>
        localStorage.getItem("isAuth") === "true" ? (
          <MyComponent {...props} />
        ) : (
          <Redirect to="/login" />
        )
      }
    />
  );
}

export default PrivateRoute;
